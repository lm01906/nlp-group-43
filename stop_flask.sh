#!/bin/bash

# Find the PID of the Flask application using lsof
FLASK_PID=$(lsof -t -i :8080)

if [ -z "$FLASK_PID" ]; then
  echo "Flask application is not running on port 8080"
else
  # Terminate the Flask application
  kill $FLASK_PID

  # Wait for a moment to ensure the process has been terminated
  sleep 2

  # Check if the process is still running
  if lsof -i :8080 > /dev/null; then
    echo "Failed to terminate Flask application, forcing termination"
    kill -9 $FLASK_PID
  else
    echo "Flask application terminated successfully"
  fi
fi