#!/bin/bash

# Install required Python packages
pip3 install -r requirements.txt

# Build stage
echo "build stage"

# Convert Jupyter notebook to Python script
jupyter nbconvert --to script app.ipynb

# Check if conversion was successful
if [ ! -f app.py ]; then
  echo "Conversion failed, exiting pipeline"
  exit 1
fi

echo "build completed"

# Start Flask application in the background
echo "Starting Flask"
nohup python3 app.py &

# Give the app some time to start
sleep 10

echo "App should have started in the background"