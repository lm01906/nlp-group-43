
# COMM061 - NATURAL LANGUAGE PROCESSING - GROUP 43

## Overview

This project is designed to deploy a Flask application that utilizes the SciBERT model for various NLP tasks. The project includes a Jupyter notebook, `app.ipynb`, which is converted to a Python script during the CI/CD pipeline. The application is deployed using a CI/CD pipeline configured in GitLab.

## Table of Contents

- [Project Structure](#project-structure)
- [Requirements](#requirements)
- [Setup](#setup)
- [Usage](#usage)
- [License](#license)

## Project Structure

```
.
├── app.ipynb                # Jupyter notebook for the application
├── requirements.txt         # List of dependencies
├── pipeline.sh              # Shell script for building the project
├── .gitlab-ci.yml           # GitLab CI/CD pipeline configuration
└── README.md                # Project documentation
```

## Requirements

- Python 3.8+
- Flask
- Gunicorn
- Jupyter
- nbconvert
- PyTorch
- NumPy
- Transformers
- Datasets
- Evaluate
- Seqeval

You can find all required packages listed in `requirements.txt`.

## Setup

1. **Clone the repository:**

   ```sh
   git clone <repository-url>
   cd <repository-directory>
   ```

2. **Set up a virtual environment and install dependencies:**

   ```sh
   python3 -m venv .venv
   source .venv/bin/activate
   pip install -r requirements.txt
   ```

3. **Run the application:**

   ```sh
   nohup python3 app.py &
   ```

## Usage

1. **Converting the Jupyter Notebook:**

   Convert `app.ipynb` to a Python script:

   ```sh
   jupyter nbconvert --to script app.ipynb
   ```

2. **Running the Flask Application:**

   Ensure that `app.py` is present in the project directory after conversion. Then, start the Flask application:

   ```sh
   python3 app.py
   ```

3. **Access the Application:**

   By default, the Flask application runs on `http://127.0.0.1:8080`. Open this URL in your web browser to access the application.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.
